#!/usr/bin/env bash

# install python
sudo curl -s -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

echo 'export PATH="~/.pyenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(pyenv init -)"' >> ~/.bash_profile
echo 'eval "$(pyenv virtualenv-init -)"'  >> ~/.bash_profile

source ~/.bash_profile

echo "Setup the bash profile successfully"

pyenv install 3.6.0

pyenv virtualenv 3.6.0 latest

pyenv global latest
