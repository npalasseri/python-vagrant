# Python Language - A place to start
This repository contains a list of configurations needed for setting up a vagrant box to use Python 3.6
# How to setup a project?
There are some prerequisites to setup this project and following are the instructions for both Mac and Windows
## Mac
< Will update soon >
## Windows
Follow the instructions (Windows), the approximate size is close to 3GB.
* Download and install 
    * [Vagrant Install](https://releases.hashicorp.com/vagrant/1.9.7/vagrant_1.9.7_x86_64.msi?_ga=2.81180887.173835525.1500198812-1148559648.1500198812)
    * [VirtualBox install](http://download.virtualbox.org/virtualbox/5.1.22/VirtualBox-5.1.22-115126-Win.exe)
* Create a folder 	
	-- C:/LearningPL/Python/Workspace
* Press Window+R and type CMD 
  ```
  > cd C:/LearningPL/Python/Workspace
  > vagrant box add hashicorp/precise64  ( Choose VirtualBox type 2 and press enter )
  ```
* Open an explorer and go to "C:/LearningPL/Python/" unzip the attached ZIP file
  ```
  > vagrant up
  > vagrant ssh
  > python --version
  ```
