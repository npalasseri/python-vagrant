#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive
sudo apt-get update

# The essential libraries that python uses 
sudo apt-get install -y build-essential libbz2-dev libssl-dev libreadline-dev \
                        libsqlite3-dev tk-dev

# optional scientific package headers (for Numpy, Matplotlib, SciPy, etc.)
sudo apt-get install -y libpng-dev libfreetype6-dev   
sudo apt-get install -y curl
sudo apt-get install -y git-all

